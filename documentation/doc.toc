\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Load or speed considerations}{2}{section.2}
\contentsline {section}{\numberline {3}Serial nature of SERDAQ}{2}{section.3}
\contentsline {section}{\numberline {4}Functionality}{2}{section.4}
\contentsline {section}{\numberline {5}RDF=CSV or the reasonable data format equals CSV}{3}{section.5}
\contentsline {section}{\numberline {6}Data input requirements}{3}{section.6}
\contentsline {section}{\numberline {7}Configuration}{4}{section.7}
\contentsline {section}{\numberline {8}How does it work}{6}{section.8}
\contentsline {section}{\numberline {9}FAQ}{6}{section.9}
\contentsline {section}{\numberline {10}For developers}{8}{section.10}
