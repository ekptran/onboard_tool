\documentclass{article}

\usepackage[includeheadfoot, margin=0.7in]{geometry}
\usepackage[fontsize=10]{scrextend}
\usepackage{enumitem}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
%\usepackage{parskip}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{subfigure}
\usepackage{listings}
\usepackage{color}
\usepackage{fancyvrb}
\usepackage{titlesec}
\usepackage[hidelinks]{hyperref}

\fancyhf{}
\pagestyle{fancy}
\rhead{Rocket Project at UCLA}
\lhead{\today}
\chead{SERDAQ}
\cfoot{\thepage}
\renewcommand{\headrulewidth}{0.4pt}

\setlist{nosep}
\linespread{1.7}

\definecolor{mykeyword}{rgb}{0,0.6,0}
\definecolor{mycomment}{rgb}{0,0.6,0.9}
\definecolor{mynumber}{rgb}{0.5,0.5,0.5}
\definecolor{mystring}{rgb}{1,0,0}

\lstset{
  language=C,
  basicstyle=\fontsize{1}{2}\selectfont,
  numbers=left,
  stepnumber=1,
  numbersep=4pt,
  morekeywords={single, class, public, private, using, namespace},
  commentstyle=\color{mycomment},
  backgroundcolor=\color{white},
  keywordstyle=\color{mykeyword},
  numberstyle=\tiny\color{mynumber},
  stringstyle=\color{mystring},
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  tabsize=2,
  captionpos=b,
  breaklines=true,
  breakatwhitespace=true,
  title=\lstname,
}

\title{SERDAQ documentation}
\author{Robert Dyro}
\date{\today}

\titlespacing{\section}{0pt}{\parskip}{-\parskip}
\titlespacing{\subsection}{0pt}{\parskip}{-\parskip}
\titlespacing{\subsubsection}{0pt}{\parskip}{-\parskip}

\begin{document}
\clearpage\maketitle
\thispagestyle{empty}
\tableofcontents
\newpage

\noindent Welcome to the SERDAQ (SERial Data AcQuisiton tool).

\section{Introduction}
This is a Unix program for data acquisition. Its application is general and not
limited to specific sensor or a system. It does, however, enforce a certain
(reasonable) convention for data acquisition.

\section{Load or speed considerations}
SERDAQ is efficient AND very fast (computational fluid dynamics programs are
efficient, but not fast). It is OK to run a dozen SERDAQs at the same time.

\section{Serial nature of SERDAQ}
SERDAQ is a serial based tool meaning it obtains data through the serial port.
A hardware serial port is how an Arduino appears to a computer. A serial port
is similar in operation to a Unix pipe. Any Unix computer with a USB can have a
serial port by buying a cheap converter.

\section{Functionality}
SERDAQ reads serial data coming through the serial port and converts that into
several useful forms. SERDAQ can do the following with the data

\begin{enumerate}
\item log to disk in raw form (but after format verification) to prevent loss
\item send data via a UDP socket to any network host
\item output the most recent data using a TCP server
\item save the data into a MySQL database
\end{enumerate}
That means that
\begin{enumerate}
\item You'll never have to worry about losing data, if all else fails, you have
a hard copy with a high resolution of saving (loss up to only 10 ms!)

\item You can just write a plotting program yourself and make it listen on a
UDP socket, receiving all the data! Or you can have another computer listen and
store the data on its own disk (if you're worried about losing the computer
running SERDAQ)

\item You can just point your browser to SERDAQ's computer ip and a chosen port
and you'll get a nice list of the most recent data. You can write a plotting
program compatible with that!

\item If your application uses a database to store and look up data (very
smart!), then you're in luck! SERDAQ can do databases.
\end{enumerate}

\section{RDF=CSV or the reasonable data format equals CSV}

There are many ways of sending and storing sensor data. It can either by binary
or text. Binary is a series of zeros and ones, text is representing numbers as
series of digits 0 through 9. Even though binary is often more compact, that
advantage does not compare with the fact that text can be easily read by
humans. Thus, SERDAQ uses text for storing and conveying data.

How do you represent a series of data where every couple milliseconds you get a
sensor value or values? After all each packet consists of time stamp and
several other values. The answer is, place them in a line and separate the
values by commas. And what do you get when you put those lines, one under the
other in a file? You get a CSV, comma separated values file. 

CSV is just text, it could have a .txt extension. The main idea is how this
text is represented. Each packet on a separate line and each value in a packet
separated by a comma. This is an example.

\begin{verbatim}
primes,fibbonaci,x^2,(-1)^n
2,1,1,-1
3,1,4,1
5,2,9,-1
7,3,16,1
11,5,25,-1
13,8,36,1
\end{verbatim}
\noindent The column description line at the top is optional, but useful.

Since CSV is simple, efficient and readable, SERDAQ expects the data to come in
as a CSV (without the description line). (Any Arduino can output the CSV format
with ease. Take a look at the "sprintf" function or get creative with
"Serial.print")


\section{Data input requirements}
SERDAQ is a data acquisition software designed for discreet time series (in
other words series sensor values tied to a specific point in time). Thus, it
requires the serial input it operates on to consists of lines with the value of
relative time (device time, could be actual, but could just be an incremental
variable) followed by 1 to about 40 max fields with data (comma separated).

Example:
\begin{verbatim}
15232,1,-23123,34732.2,0349,384
\end{verbatim}

The lines don't have to come whole at separated intervals (serial doesn't
support packeted transmissions). SERDAQ will detect and parse the lines
automatically.

Serial connection contains no error checking so SERDAQ performs it
automatically. However, the user has to specify the expected number of fields
in the configuration file.

\section{Configuration}
SERDAQ operation depends on several variables such as the operating system path
to the serial device, user specified number of fields in each data line, which
of the functionality to use, whether to be verbose or produce no output, etc.

Thus, invoking SERDAQ requires configuration via in-program prompts before
starting data acquisition or writing a configuration file and piping it into
SERDAQ.

Piping [a file] into a Unix program is as easy as doing the following
\begin{verbatim}
$ serdaq < configuration_file.txt
\end{verbatim}
Notice the "\verb|<|" symbol. Note: this will NOT work
\begin{verbatim}
$ configuration_file.txt > serdaq # WRONG
\end{verbatim}

A configuration file consits of lines with a single configuration per line. The
file ends with a word "done" by itself on the last line. Each configuration
consists of a keyword and a value. Keyword and value are separated by equals
sign, "=".

It is probably best to simply list all the options and discuss them along doing
so.

\begin{table}[H]
\begin{tabular}{p{3cm} | p{4cm} | p{10cm}}
Option (keyword)        & Value (setting)           & Description \\ \hline
use mysql               & \verb|1| or \verb|0| (for yes or no)    & decide whether to use the databases functionality \\ \hline
mysql address           & e.g. \verb|127.0.0.1|            & specify the network address of the database \\ \hline
mysql database          & e.g. \verb|my_database|          & specify which database to use \\ \hline
mysql table             & e.g. \verb|thermo_data|          & name of table to use in the specified database \\ \hline
mysql login             & e.g. \verb|root|                 & mysql login \\ \hline
mysql password          & e.g. \verb|my_password123| & mysql password \\ \hline
use log                 & \verb|1| or \verb|0| (for yes or no)    & write data to disk? \\ \hline
log directory           & e.g. \verb|/home/documents|      & directory for logs \\ \hline
use server              & \verb|1| or \verb|0| (for yes or no)    & whether to use TCP server functionality \\ \hline
server port             & e.g. \verb|8888|                 & TCP port on which to set the server up \\ \hline
serial path             & e.g. \verb|/dev/ttyAMA0|         & path to the serial device, the data source \\ \hline
serial speed            & e.g. \verb|-1| (for default)     & specify serial device speed, usually default is good, but can be \verb|9600| for instance  \\ \hline
model                   & e.g. \verb|rel_t,a0,a1,a2,a3|    & CSV line of data column names corresponding to number of data fields, used for error checking so if this doesn't match the actual data, then data will be discarded \\ \hline
print output            & \verb|1| or \verb|0| (for yes or no)    & decide whether output should be printed \\ \hline
use host                & \verb|1| or \verb|0| (for yes or no)    & turns UDP broadcasting functionality on or off \\ \hline
host                    & e.g. \verb|192.168.1.55:7777|    & choose the receiving host for the UDP data broadcast \\
\end{tabular}
\end{table}

Here's an example of a configuration file.
\begin{verbatim}
use mysql = 1 
mysql address = 127.0.0.1
mysql database = serial_tool
mysql login = root
model = rel_t,a0,a1,a2,a3,a4,a5
mysql password = pinetree
mysql table = pins
use log = 1 
log directory = ./
use server = 1 
server port = 8888
serial path = ./my_pipe
serial speed = -1
print_output = 1
done
\end{verbatim}


\section{How does it work}

SERDAQ works by splitting the work into several threads so that it can do
several things at a time and be everywhere on time.

SERDAQ uses POSIX threads to separate the workload of obtaining data from the
serial port, logging it, sending it via UDP, offering it via a TCP server and
storing it in a database.

SERDAQ uses threads to ensure real-time response, it does not use all of the
operating system computational time. In fact, despite working hard, threads of
SERDAQ mostly sleep.

When full functionality is selected, SERDAQ uses 5 threads.
\begin{itemize}
\item main thread for policing other threads and outputting real-time status
\item serial thread for obtaining the data via the serial port (real-time)
\item logging thread for logging data to disk and sending the data via UDP
\item server thread for making the latest data available to any client
\item database thread for storing the data in a database
\end{itemize}

Since all data handling and storing operations can result in missing new data
(be it only couple milliseconds), one thread is devoted entirely to obtaining
new data and making it available to data handling threads.

Since new data comes in as sequence and has to be handled on a
first-in-first-out basis, data is stored using a queue data structure. The
particular chosen implementation of a queue is a C linked list. Data is
appended on one end and handled on the other.

\section{FAQ}
Q: I have several sensors outputting several different values each and running
at different data rates on different microprocessors. How do I lump them
together for SERDAQ?\\
A: Please don't. If you have several sensors then use several serial devices
and run several SERDAQ's at the same time. If you do, you stand a chance of
making your system more understandable, more robust and more independent.

If you're wondering how to run several SERDAQs at the same time, here's a
bash script.
\begin{verbatim}
#!/bin/bash
serdaq < config1 & # run serdaq unhooked (in the background)
export serdaq1_pid=$$
serdaq < config2 &
export serdaq2_pid=$$
serdaq < config3 &
export serdaq3_pid=$$
\end{verbatim}
Save it and run like so:
\begin{verbatim}
$ . script_name.sh # notice the dot 
\end{verbatim}
Then, once you're ready to stop all those SERDAQ's just do this
\begin{verbatim}
$ kill serdaq1_pid
$ kill serdaq2_pid
$ kill serdaq3_pid
\end{verbatim}
\noindent\rule{8cm}{0.4pt}\\
Q: SERDAQ doesn't log any data, what do I do?\\
A: First of all, don't panic. Computers always do exactly what they're told.
\begin{itemize}
\item Start by reexamining your configuration file.
\item Consider running without the silent mode so that you can see real-time
status of everythig.
\item Run SERDAQ by itself (without the configuration file) and input the
options by hand. Use can you "print" to print current configuration
status. Once you're done type "done" and press enter twice.
\item Try the command "cat" on the serial port, see what data are you getting
exactly.
\item Make sure the serial port permissions are such that you (the user) can
actually read and write to the serial port.
\end{itemize}
\noindent\rule{8cm}{0.4pt}\\
Q: I can't compile SERDAQ beacuse \verb|mysql_config| was not found.\\
A: SERDAQ has the ability to store data in a database, but that comes at a
cost: it depends on mysql development library. You can either install the
library or forfeit the database option and compile without it:
\begin{verbatim}
$ make without-mysql
\end{verbatim}
\noindent\rule{8cm}{0.4pt}\\
Q: How do I...\\
A: Whoa, whoa! Let's talk about it. Email me at \verb|robert.dyro@gmail.com|

\section{For developers}
SERDAQ's code is fairly complicated to be able to be generally applicable, fast
and have an easy interface. It is advised that anyone willing to develop has a
good understanding of POSIX threads.

There are several places in the code that are often visited in development
because of their criticality. For this reason, commented code has been placed
at those locations enabling debugging features (mainly verbose output).

For any questions or comments, contact:
\verb|robert.dyro@gmail.com|

\end{document}
