## WELCOME to (OTDA) Onboard Tool for Data Acquistion

This is the serial tool to be used on embedded microprocessors.

## BRIEF DESCRIPTION

This is a Unix program for data acquisition. Its application is general and not
limited to specific sensor or a system. It does, however, enforce a certain
(reasonable) convention for data acquisition.

## SERIAL NATURE OF THE OTDA

OTDA is a serial based tool meaning it obtains data through the serial port.
A hardware serial port is how an Arduino appears to a computer. A serial port
is similar in operation to a Unix pipe. Any Unix computer with a USB can have a
serial port by buying a cheap converter.

## FUNCTIONALITY

OTDA reads serial data coming through the serial port and converts that into
several useful forms. OTDA can do the following with the data

1. log to disk in raw form (but after format verification) to prevent loss
2. send data via a UDP socket to any network host
3. output the most recent data using a TCP server
4. save the data into a MySQL database

That means that

1. You'll never have to worry about losing data, if all else fails, you have a
hard copy with a high resolution of saving (loss up to only 10 ms!)

2. You can just write a plotting program yourself and make it listen on a UDP
socket, receiving all the data! Or you can have another computer listen and
store the data on its own disk (if you're worried about losing the computer
running OTDA)

3. You can just point your browser to OTDA's computer ip and a chosen port
and you'll get a nice list of the most recent data. You can write a plotting
program compatible with that!

4. If your application uses a database to store and look up data (very
smart!), then you're in luck! OTDA can do databases.

## RDF=CSV OR THE REASONABLE DATA FORMAT EQUALS CSV

There are many ways of sending and storing sensor data. It can either by binary
or text. Binary is a series of zeros and ones, text is representing numbers as
series of digits 0 through 9. Even though binary is often more compact, that
advantage does not compare with the fact that text can be easily read by
humans. Thus, OTDA uses text for storing and conveying data.  (Recommended 
store in .csv form)

This is an example.

```
primes,fibbonaci,x^2,(-1)^n
2,1,1,-1
3,1,4,1
5,2,9,-1
7,3,16,1
11,5,25,-1
13,8,36,1
```

The column description line at the top is optional, but useful.

Since CSV is simple, efficient and readable, OTDA expects the data to come in
as a CSV (without the description line). (Any Arduino can output the CSV format
with ease. Take a look at the "sprintf" function or get creative with
"Serial.print")


## DATA INPUT REQUIREMENT

OTDA is a data acquisition software designed for discreet time series (in
other words series sensor values tied to a specific point in time). Thus, it
requires the serial input it operates on to consists of lines with the value of
relative time followed by 1 to about 40 max fields with data (comma separated).

Example:
```
15232,1,-23123,34732.2,0349,384
```

The lines don't have to come whole at separated intervals (serial doesn't
support packeted transmissions). OTDA will detect and parse the lines
automatically.

Serial connection contains no error checking so OTDA performs it
automatically. However, the user has to specify the expected number of fields
in the configuration file.

## CONFIGURATION

Invoking OTDA requires configuration via in-program prompts before
starting data acquisition or writing a configuration file and piping it into
OTDA.

Piping [a file] into a Unix program is as easy as doing the following
$ OTDA < configuration_file.txt
Notice the "<" symbol. Note: this will NOT work
$ configuration_file.txt > OTDA # WRONG

A configuration file consits of lines with a single configuration per line. The
file ends with a word "done" by itself on the last line. Each configuration
consists of a keyword and a value. Keyword and value are separated by equals
sign, "=".

It is probably best to simply list all the options and discuss them along doing
so.

Option (keyword)        | Value (setting)           | Description
------------------------|---------------------------|--------------------------
use mysql               | 1 or 0 (for yes or no)    | decide whether to use the databases functionality
mysql address           | e.g. 127.0.0.1            | specify the network address of the database
mysql database          | e.g. my_database          | specify which database to use
mysql table             | e.g. thermo_data          | name of table to use in the specified database
mysql login             | e.g. root                 | mysql login
mysql password          | e.g. my_password123       | mysql password
use log                 | 1 or 0 (for yes or no)    | write data to disk?
log directory           | e.g. /home/documents      | directory for logs
use server              | 1 or 0 (for yes or no)    | whether to use TCP server functionality
server port             | e.g. 8888                 | TCP port on which to set the server up
serial path             | e.g. /dev/ttyAMA0         | path to the serial device, the data source
serial speed            | e.g. -1 (for default)     | specify serial device speed, usually default is good, but can be 9600 for instance
model                   | e.g. rel_t,a0,a1,a2,a3    | CSV line of data column names corresponding to number of data fields, used for error checking so if this doesn't match the actual data, then data will be discarded
print output            | 1 or 0 (for yes or no)    | decide whether output should be printed
use host                | 1 or 0 (for yes or no)    | turns UDP broadcasting functionality on or off
host                    | e.g. 192.168.1.55:7777    | choose the receiving host for the UDP data broadcast

Here's an example of a configuration file.
```
use mysql = 1 
mysql address = 127.0.0.1
mysql database = serial_tool
mysql login = root
model = rel_t,a0,a1,a2,a3,a4,a5
mysql password = pinetree
mysql table = pins
use log = 1 
log directory = ./
use server = 1 
server port = 8888
serial path = ./my_pipe
serial speed = -1
print_output = 1
use host = 1
host = linux.local:6666
done
```


## HOW DOES IT WORK

OTDA works by splitting the work into several threads so that it can do
several things at a time and be everywhere on time.

OTDA uses POSIX threads to separate the workload of obtaining data from the
serial port, logging it, sending it via UDP, offering it via a TCP server and
storing it in a database.

OTDA uses threads to ensure real-time response, it does not use all of the
operating system computational time. In fact, despite working hard, threads of
OTDA mostly sleep.

When full functionality is selected, OTDA uses 5 threads.
* main thread for policing other threads and outputting real-time status
* serial thread for obtaining the data via the serial port (real-time)
* logging thread for logging data to disk and sending the data via UDP
* server thread for making the latest data available to any client
* database thread for storing the data in a database

Since all data handling and storing operations can result in missing new data
(be it only couple milliseconds), one thread is devoted entirely to obtaining
new data and making it available to data handling threads.

Since new data comes in as sequence and has to be handled on a
first-in-first-out basis, data is stored using a queue data structure. The
particular chosen implementation of a queue is a C linked list. Data is
appended on one end and handled on the other.


Contributors

Ethan Tran
Bryan Le
Alex Nguyen