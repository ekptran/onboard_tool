var searchData=
[
  ['server_5faccept_5ferror',['SERVER_ACCEPT_ERROR',['../serdaq_8h.html#a515c2857f889697099d74ba638ea1b3e',1,'serdaq.h']]],
  ['server_5flisten_5ferror',['SERVER_LISTEN_ERROR',['../serdaq_8h.html#ab87ab2fd51308a83b70a23cb970749fc',1,'serdaq.h']]],
  ['server_5fnon_5fblocking_5ferror',['SERVER_NON_BLOCKING_ERROR',['../serdaq_8h.html#a30e0289907d45c98615fa1552f344700',1,'serdaq.h']]],
  ['server_5freceive_5fzero_5ferror',['SERVER_RECEIVE_ZERO_ERROR',['../serdaq_8h.html#ad81327cdb3be325a842669b1f53c2de9',1,'serdaq.h']]],
  ['server_5fsocket_5fbind_5ferror',['SERVER_SOCKET_BIND_ERROR',['../serdaq_8h.html#a984f89111917399e32b7eb08d835425d',1,'serdaq.h']]],
  ['server_5fsocket_5fcreate_5ferror',['SERVER_SOCKET_CREATE_ERROR',['../serdaq_8h.html#a4f670e8810d329ddd71f1639590a3a8d',1,'serdaq.h']]]
];
