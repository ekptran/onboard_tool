var searchData=
[
  ['canvas',['canvas',['../structtext__plot__config.html#a02f894032d5ada0173f40426dcb454a6',1,'text_plot_config']]],
  ['change_5fport_5fpermissions',['change_port_permissions',['../main__thread_8h.html#ad5095d05b2c77735338093be00af3931',1,'main_thread.h']]],
  ['clean_5fexit_5ferror',['CLEAN_EXIT_ERROR',['../serdaq_8h.html#a2b3ff2b56174c954430c6848a846180d',1,'serdaq.h']]],
  ['clear',['clear',['../structtext__plot__config.html#ad1f1fb112bf8ef4265e74af2ec54a34d',1,'text_plot_config']]],
  ['connect_5fto_5fmysql',['connect_to_mysql',['../log__and__db__data__handling_8h.html#ac793e488d3d0e622cff4fc7f95f81a9f',1,'log_and_db_data_handling.h']]],
  ['csv_5fparse_2eh',['csv_parse.h',['../csv__parse_8h.html',1,'']]]
];
