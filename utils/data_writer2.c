#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <fcntl.h>

#define N 2048

int writefd;
char running = 1;

void exit_safely(int signum) {
  running = 0;
}

double current_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);

  return ((double)tv.tv_sec) + ((double)tv.tv_usec) / 1e6;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    exit(1);
  }

  signal(SIGINT, exit_safely);
  signal(SIGPIPE, SIG_IGN);
  writefd = open(argv[1], O_WRONLY | O_NONBLOCK);

  char buffer[N];
  double t;
  double vals[6];
  int i;
  while (running) {
    t = current_time();
    for (i = 0; i < 6; i++) {
      vals[i] = sin(t + (2.0 * M_PI) / 6.0 * (double)i);
    }
    sprintf(buffer, "%f,%f,%f,%f,%f,%f,%f\n", t, vals[0], vals[1], vals[2], vals[3], vals[4], vals[5]);
    //int bytes = write(STDOUT_FILENO, buffer, strlen(buffer));
    int bytes = write(writefd, buffer, strlen(buffer));
    if (bytes < 0)
      writefd = open(argv[1], O_WRONLY | O_NONBLOCK);
    //printf("%d\n", bytes);
    usleep(1000 * 10);
  }
  close(writefd);
}
