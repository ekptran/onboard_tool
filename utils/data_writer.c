#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>

char* message;
int fd_ser2cli;

uint64_t micros() {
  struct timeval tv;  
  gettimeofday(&tv, NULL);
  return (((uint64_t)tv.tv_sec) * 1000000LLU) + ((uint64_t)tv.tv_usec);
}

void delay(uint64_t millisec) {
  struct timespec tv;
  tv.tv_sec = millisec / 1000LLU;
  tv.tv_nsec = (millisec % 1000LLU) * 1000000LLU;
  nanosleep(&tv, NULL);
}

void exit_nicely(int sig) {
  free(message);
  close(fd_ser2cli);
  exit(0);
}

void handle_broken_pipe(int sig) {
}

int main(int argc, char** argv) {
  signal(SIGINT, exit_nicely);
  signal(SIGPIPE, handle_broken_pipe);

  message = malloc(sizeof(char));

  long long unsigned start = micros();
  srand(time(NULL));
  mkfifo("./my_pipe", 0666);

  fd_ser2cli = open("./my_pipe", O_WRONLY | O_NONBLOCK);

  char number[256];
  while (1) {
    if (fd_ser2cli == -1)
      fd_ser2cli = open("./my_pipe", O_WRONLY | O_NONBLOCK);

    sprintf(message, "%llu", micros() - start);
    int i;
    for (i = 0; i < 6; i++) {
      sprintf(number, ",%d", rand() % 4096);
      strcat(message, number);
    }
    strcat(message, "\n");
    int r = write(fd_ser2cli, message, strlen(message));
    //printf("Write returned %d\n", r);
    delay(1);
  }

  exit_nicely(1);
}

