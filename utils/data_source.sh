#!/bin/bash

# create a pipe, software equivalent of a serial port
mkfifo my_pipe 

i=0 # intialize relative time mimicking variable

# insert data into pipe forever, until Ctrl+C is used
# insert in the form rel_t,a0,a1,a2,a3,a4,a5
# where a# stands for value from a sensor
while true; do 
  echo $i,"$RANDOM","$RANDOM",-"$RANDOM","$RANDOM","$RANDOM","$RANDOM" > ./my_pipe;
  sleep 0.00001;
  i=$((i+1))
done
