CC=gcc
CFLAGS=-Wall -std=gnu99
EXEC=serdaq
LIBS = -lm -lpthread
FILES=$(wildcard src/*.c)
HEADERS=$(wildcard src/*.h)
OBJS=$(patsubst %.o,obj/%.o, $(patsubst %.c,%.o,$(FILES:src/%=%)))
OBJS_NODB=$(patsubst %.o,obj_nodb/%.o, $(patsubst %.c,%.o,$(FILES:src/%=%)))

obj/%.o: src/%.c $(HEADERS)
	$(CC) -c $< -o $@ -DUSE_MYSQL=1 $(CFLAGS) $$(mysql_config --cflags --libs) $(LIBS)

obj_nodb/%.o: src/%.c $(HEADERS)
	$(CC) -c $< -o $@ -DUSE_MYSQL=0 $(CFLAGS) $(LIBS)

all: $(OBJS) $(HEADERS)
	$(CC) -o $(EXEC) $(CFLAGS) $(OBJS) $$(mysql_config --cflags --libs) -DUSE_MYSQL=1 $(LIBS)

without-mysql: $(OBJS_NODB) $(HEADERS)
	$(CC) -o $(EXEC) $(CFLAGS) $(OBJS_NODB) -DUSE_MYSQL=0 $(LIBS)

clean:
	rm -f $(OBJS)
	rm -f $(OBJS_NODB)
	rm -f $(EXEC)
	rm -rf $(EXEC).dSYM

doc:
	doxygen Doxyfile
