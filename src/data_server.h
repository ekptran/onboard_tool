#ifndef DATA_SERVER_H
#define DATA_SERVER_H

#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>
#include <signal.h>
#include <pthread.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

#include "serdaq.h"
#include "atomic_read_write_tools.h"

/*! \file data_server.h
    \brief Fully defined, basic TCP server.
*/


/*! \fn die(char* s)
    \brief Exits the server safely in case of an error.
    \param s Error message as a c string.
*/
void die(char* s);

/*! \fn handleClntSock(int clntSock, char* body, size_t body_size)
    \brief Handles a single client connection, response building and responding.

    \param clntSock Client socket to respond to.
    \param body Body of the message to send to client. C string.
    \param body_size the size of the c string, body.
*/
void handleClntSock(int clntSock, char* body, size_t body_size);

/*! \fn server(void* arg)
    \brief Server thread defining function.

    \param arg Thread argument. Unused, but required to define a thread.
*/
void* server(void* arg);

/*! \fn build_a_recent_list(char* body, size_t body_n, str_node** str_list, uint64_t milliseconds)
    \brief Function building the response to client based on recent data.

    A function which converts recent data stored in the data linked list into a
    c string to be sent to the client. The response consists of a CSV file with
    a first row of column names, followed by data at at most 100 Hz.

    \param body c string to place the response into, already allocated
    \param body_n max size of the body c string
    \param str_list the head of the data linked list
    \param milliseconds the last number of milliseconds to consider in data
    \return the number meaningful of bytes in the body c string
*/
size_t build_a_recent_list(char* body, size_t body_n, str_node** str_list, uint64_t milliseconds);

#endif
