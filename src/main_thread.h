#ifndef MAIN_THREAD_H
#define MAIN_THREAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
//#include <fcntl.h>   /* File control definitions */
//#include <errno.h>   /* Error number definitions */
//#include <termios.h> /* POSIX terminal control definitions */
#include <signal.h>
//#include <sys/time.h>
//#include <time.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
//#include <mysql/mysql.h>
#include <stdint.h>
#include <pthread.h>
#include <ctype.h>

#include "atomic_read_write_tools.h"
#include "text_plot.h"
#include "serdaq.h"


#define PORT_NAME "/dev/my_pipe"
#define PORT_SPEED -1

/*! \file main_thread.h
    \brief Functions containing program's entry point and policing thread.
*/

/*! \struct graph
    \brief Data structure holding information for the graph of recent data.

    \var graph::x
    \brief time array
    \var graph::Y
    \brief array of value arrays
    \var graph::M
    \brief number of Y data series
    \var graph::N
    \brief number of data points
    \var graph::last_updated
    \brief time stamp of last update
    \var graph::minx
    \brief minimum value in x
    \var graph::maxx
    \brief maximum value in x
    \var graph::miny
    \brief minimum value in y
    \var graph::maxy
    \brief maximum value in y
*/
typedef struct {
  double* x;
  double** Y;
  int M;
  int N;
  double last_updated;
  double minx;
  double maxx;
  double miny;
  double maxy;
} graph;


/*! \fn sighandler(int signum)
    \brief Signal handler designed to cause the program to exit cleanly.

    Sets global variable "running" to false (0) causing all threads to exit.

    \param signum Parameter required by a POSIX signal handler.
*/
void sighandler(int signum);

/*! \fn add_soc_node(soc_node* soc, char* ip, int port)
    \brief Appends recipient host to the list of receipients. Can use hostname.

    \param soc the tail of the receipient host list
    \param ip hostname or ip as a c string
    \param port the port to which to send UDP packets
    \return soc_node the new tail of the host receipients list
*/
soc_node* add_soc_node(soc_node* soc, char* ip, int port);

/*! \fn free_soc_nodes(soc_node* soc)
    \brief Frees the list of the host recipients.

    \param soc the head of the list of receipients to be deallocated
*/
void free_soc_nodes(soc_node* soc);

/*! \fn change_port_permissions(char* serial_path)
    \brief Changes serial port file system permissions to make it readable.

    \param serial_path path to the serial device in the file system
*/
void change_port_permissions(char* serial_path);

/*! \fn trim_whitespace(char* str)
    \brief Trims any number of white space from the beginning and end of string.
    
    \param str string to be trimmed, the string is modified
    \return the pointer to the new string within the old string
*/
char* trim_whitespace(char* str);

/*! \fn my_isspace(char* c)
    \brief Custom function for checking if a character is a whitespace.

    \param c pointer to the character to check
    \return 1 or 0 depending on whether character c is a space
*/
int my_isspace(char* c);

/*! \fn write_status_page()
    \brief Prints the single instance of the dynamically built status page.
*/
void write_status_page();

/*! \fn print_configuration()
    \brief Prints the current configuration to the terminal.
*/
void print_configuration();

/*! \fn process_command(char* field, char* value)
    \brief Processes a command consisting of a field and value. Sets parameters.

    \param field keyword or the configuration name
    \param value value or the specific setting for the given configuration (key)
*/
void process_command(char* field, char* value);

#endif
