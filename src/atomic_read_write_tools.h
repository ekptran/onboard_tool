#ifndef ATOMIC_READ_AND_WRITE_TOOLS_H
#define ATOMIC_READ_AND_WRITE_TOOLS_H

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <stdint.h>

/*! \file atomic_read_write_tools.h
    \brief A set of functions allowing for atomic reading and writing.
*/

extern pthread_mutex_t atomic64;
extern pthread_mutex_t atomic32;

/*! \fn atomicRead64(int64_t* i)
    \brief Function allowing for reading a variable by one thread at a time.

    64-bit version

    \param i pointer to the variable to be read atomically
    \return the value of the variable, read atomically
*/
int64_t atomicRead64(int64_t* i);

/*! \fn atomicWrite64(int64_t* i, int64_t v)
    \brief Function allowing for writing to a variable by one thread at a time.

    64-bit version

    \param i pointer to the variable to be written to atomically
    \param v value to write into atomically
*/
void atomicWrite64(int64_t* i, int64_t v);

/*! \fn atomicAdd64(int64_t* i, int64_t v)
    \brief Function allowing to add an integer value to a variable atomically.

    64-bit version. Note that the value added can be negative.

    \param i pointer to the variable to be changed atomically
    \param v value to add to the variable atomically
    \return new value of the variable changed
*/
int64_t atomicAdd64(int64_t* i, int64_t v);

void atomicSafeUnlock64();

/*! \fn atomicRead32(int32_t* i)
    \brief Function allowing for reading a variable by one thread at a time.

    32-bit version

    \param i pointer to the variable to be read atomically
    \return the value of the variable, read atomically
*/
int32_t atomicRead32(int32_t* i);

/*! \fn atomicWrite32(int32_t* i, int32_t v)
    \brief Function allowing for writing to a variable by one thread at a time.

    32-bit version

    \param i pointer to the variable to be written to atomically
    \param v value to write into atomically
*/
void atomicWrite32(int32_t* i, int32_t v);

/*! \fn atomicAdd32(int32_t* i, int32_t v)
    \brief Function allowing to add an integer value to a variable atomically.

    32-bit version. Note that the value added can be negative.

    \param i pointer to the variable to be changed atomically
    \param v value to add to the variable atomically
    \return new value of the variable changed
*/
int32_t atomicAdd32(int32_t* i, int32_t v);

void atomicSafeUnlock32();

#endif
