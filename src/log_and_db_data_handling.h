#ifndef LOG_AND_DB_DATA_HANDLING_H
#define LOG_AND_DB_DATA_HANDLING_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>  
//#include <unistd.h>  
//#include <fcntl.h>   
//#include <errno.h>   
//#include <termios.h> 
#include <signal.h>
//#include <sys/time.h>
//#include <time.h>
#include <sys/socket.h>
//#include <netdb.h>
//#include <netinet/in.h>
#include <sys/types.h>
#if USE_MYSQL==1
#include <mysql/mysql.h>
//#include <mysql.h>
#endif
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

#include "atomic_read_write_tools.h"
#include "serdaq.h"

#define MAX_LOG_N 256

/*! \file log_and_db_data_handling.h
    \brief Functions defining logging and database functionality.
*/

/*! \fn open_new_log(char* path_to_log_dir) 
    \brief Opens a new log file (for instance if the old one is too big).

    \param path_to_log_dir path to the directory in which logs are stored
    \return file descriptor to the newly opened log or NULL if error
*/
FILE* open_new_log(char* path_to_log_dir);

/*! \fn free_str_nodes(str_node* str)
    \brief Frees a data linked list.

    \param str head of the data linked list to be freed
*/
void free_str_nodes(str_node* str);

/*! \fn handle_mysql_str_list(str_node** head_ptr)
    \brief Parses and stores data linked list nodes into a database.

    \param head_ptr pointer to the head of the mysql data linked list
*/
void handle_mysql_str_list(str_node** head_ptr);

/*! \fn handle_log_str_list(str_node** head_ptr)
    \brief Parses and stores data linked list into a file. Also does UDP broadcast.

    \param head_ptr pointer to the head of the log data linked list
*/
void handle_log_str_list(str_node** head_ptr);

/*! \fn handle_mysql_data(void* arg)
    \brief Thread defining function for mysql database functionality.

    \param arg argument required from a POSIX thread
    \return return required from a POSIX thread
*/
void* handle_mysql_data(void* arg);

/*! \fn handle_log_data(void* arg)
    \brief Thread defining function for logging functionality.

    \param arg argument required from a POSIX thread
    \return return required from a POSIX thread
*/
void* handle_log_data(void* arg);

/*! \fn connect_to_mysql(int init)
    \brief Connects to mysql databases and does initial configuration.

    \param init flag indicating whether this is the first or a subsequent connection
*/
void connect_to_mysql(int init);
#endif
