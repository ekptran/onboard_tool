#ifndef CSV_PARSE_H
#define CSV_PARSE_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*! \file csv_parse.h
    \brief Basic CSV parsing functionality.
*/

/*! \fn parse_CSV(char* csv, char*** fields)
    \brief Parses a c string into an array of c strings. Splits by commas.

    This function allocates memory for the fields. All it needs is a pointer to
    an array of c strings to be passed to it.

    \param csv c_string, a line of a CSV to be parsed into fields
    \param fields a pointer to an array of c strings
    \return the number of fields in a line
*/
int parse_CSV(char* csv, char*** fields);

/*! \fn free_CSV_fields(char** fields, int n)
    \brief Frees CSV fields (an array of c strings) allocated by the parsing function.

    \param fields array of c strings, fields of a parsed CSV line
    \param n number of parsed fields in the array of the c strings
*/
void free_CSV_fields(char** fields, int n);

#endif
