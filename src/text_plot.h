#ifndef TEXT_PLOT_H
#define TEXT_PLOT_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

/*! \file text_plot.h
    \brief defines functions for plotting in text 
*/

/*! \struct point2D 
    \brief A tuple of two doubles for sorting two arrays based on one.
*/
typedef struct {
  double a;
  double b;
} point2D;


/*! \struct text_plot_config 
    \brief A set of configuration values for plotting.

    \var text_plot_config::canvas 
    \brief A two dimensional array of characters representing the graph.
    \brief \var text_plot_config::x
    \brief Indepedent variable array to imprint on the graph.
    \brief \var text_plot_config::y
    \brief Dependent variable array to imprint on the graph.
    \brief \var text_plot_config::N
    \brief Length of x and y arrays.
    \brief \var text_plot_config::minx
    \brief Minimal value of x in the array. Pass by caller for efficiency.
    \brief \var text_plot_config::maxx
    \brief Maximal value of x in the array. Pass by caller for efficiency.
    \brief \var text_plot_config::miny
    \brief Minimal value of y in the hrray. Pass by caller for efficiency.
    \var text_plot_config::maxy
    \brief Maximal value of y in array. Pass by caller for efficiency.
    \brief \var text_plot_config::free
    \brief Whether to free the canvas or not.
    \var text_plot_config::clear
    \brief Whether to clear the canvas or not.
    \brief \var text_plot_config::draw
    \brief Whether to print the canvas to terminal or just draw on canvas.
    \var text_plot_config::n
    \brief Number of rows of the canvas.
    \var text_plot_config::m
    \brief Number of columns of the canvas.
    \var text_plot_config::marker
    \brief Marker type for data. For example '#', '+', 'x', '.'
*/
typedef struct {
  char* canvas;
  double* x;
  double* y;
  int N;
  double minx;
  double maxx;
  double miny;
  double maxy;
  char free;
  char clear;
  char draw;
  int n;
  int m;
  char marker;
} text_plot_config;


/*! \fn text_plot(text_plot_config* config)
    \brief Function creating a text plot from a configuration given.
    \sa text_plot_config
    \param config configuration data structure, also contains data
*/
void text_plot(text_plot_config* config);

/*! \fn ascPoint2D(const void* a, const void* b)
    \brief Helper function for comparing two points, used for sorting arrays.
    \sa point2D
    \param a left value to compare (of type point2D)
    \param b right value to compare (of type point2D)
*/
int ascPoint2D(const void* a, const void* b);

#endif /* TEXT_PLOT_H */
