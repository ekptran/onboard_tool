#ifndef SERIAL_HANDLER_H
#define SERIAL_HANDLER_H 

/* BEGIN EXTERNAL LIBRARY INCLUDES */
#include <netdb.h>

#if USE_MYSQL == 1
#include <mysql/mysql.h>
#endif
/*END EXTERNAL LIBRARY INCLUDES */


/*BEGIN INTERNAL LIBRARY INCLUDES */
#include "csv_parse.h"
/*END INTERNAL LIBRARY INCLUDES */

/*! \file serdaq.h
    \brief Program's main include file, containing global variables and funcs.
*/


/* BEGIN TYPES DEFINITIONS */
/*! \struct str_node
    \brief String node. A linked list node containing a string and time value.

    \var str_node::time
    \brief time stamp associated with a data node
    \var str_node::text
    \brief string holding data for the data node
    \var str_node::next
    \brief pointer to the next node in the linked list
*/
struct str_node {
  char text[256];
  double time;
  struct str_node* next;
};
typedef struct str_node str_node;

/*! \struct soc_node
    \brief Socket node. A linked list node containing socket of a UDP receipient.

    \var soc_node::fd
    \brief receipient UDP socket
    \var soc_node::servaddr
    \brief destination information for the recipient socket
    \var soc_node::next
    \brief pointer ot the next node in the linked list
*/
struct soc_node {
  int fd;
  struct sockaddr_in servaddr;
  struct soc_node* next;
};
typedef struct soc_node soc_node;

/*! \struct status_struct
    \brief Current status of the program. Used for policing threads.

    \var status_struct::mysql_t
    \brief time in microseconds of the last report from mysql thread
    \var status_struct::server_t
    \brief time in microseconds of the last report from server thread
    \var status_struct::log_t
    \brief time in microseconds of the last report from log thread
    \var status_struct::mysql_nodes
    \brief number of nodes in the mysql data queue
    \var status_struct::server_requests
    \brief number of requests that have been made to the server
    \var status_struct::log_nodes
    \brief number of nodes in the log data queue
    \var status_struct::mysql_error
    \brief error number of the mysql thread
    \var status_struct::log_error
    \brief error number of the log thread
    \var status_struct::server_error
    \brief error number of the server thread
    \var status_struct::pipe_error
    \brief error number of the serial handling thread
    \var status_struct::values
    \brief last data node, used for graphing
*/
struct status_struct {
  uint64_t mysql_t;
  uint64_t server_t;
  uint64_t log_t;
  uint32_t mysql_nodes;
  uint64_t server_requests;
  uint32_t log_nodes;
  uint32_t mysql_error;
  uint32_t log_error;
  uint32_t server_error;
  uint32_t pipe_error;
  str_node values;
};
typedef struct status_struct status_struct;
/* END TYPES DEFINITIONS */


/* BEGIN GLOBALS DEFINITIONS */
/*! \var running
    \brief global flag used to cleanly terminate all processes
*/
extern volatile int running;

/*! \var model
    \brief global variable holding data model
*/
extern char model[1024];

/*! \var mysql_addr
    \brief global variable holding the address of the mysql server
*/
extern char mysql_addr[256];

/*! \var mysql_login
    \brief global variable holding the login to the mysql server
*/
extern char mysql_login[256];

/*! \var mysql_pswd
    \brief global variable holding the password to the mysql server
*/
extern char mysql_pswd[256];

/*! \var mysql_table
    \brief global variable holding the name of the mysql table to use
*/
extern char mysql_table[256];

/*! \var mysql_db
    \brief global variable holdin the mysql database name to use
*/
extern char mysql_db[256];

/*! \var serial_path
    \brief the file system path to the serial device
*/
char serial_path[2048];

#if USE_MYSQL==1
/*! \var con
    \brief mysql connection variable, used for sending commands to mysql server
*/
extern MYSQL* con;
#endif

/*! \var verbose
    \brief not much used now, but used in the past for debugging, controls verbosity
*/
extern volatile int verbose;

/*! \var log_fd
    \brief current file descriptor of the currently opened log
*/
extern FILE* log_fd;

/*! \var fd
    \brief file descriptor of the serial path
*/
extern int fd;

/*! \var path_to_log_dir
    \brief path to the directory in which logs are placed
*/
extern char path_to_log_dir[4096];

/*! \var head_soc_list
    \brief the head of the linked list of the UDP receipients
*/
extern soc_node* head_soc_list;

/*! \var tail_soc_list
    \brief the tail of the linked list of the UDP receipients
*/
extern soc_node* tail_soc_list;

/*! \var head_str_list_mysql
    \brief head of the data linked list of the mysql thread
*/
extern str_node* head_str_list_mysql;

/*! \var tail_str_list_mysql
    \brief tail of the data linked list of the mysql thread
*/
extern str_node* tail_str_list_mysql;

/*! \var head_str_list_server
    \brief head of the data linked list of the server thread
*/
extern str_node* head_str_list_server;

/*! \var tail_str_list_server
    \brief tail of the data linked list of the server thread
*/
extern str_node* tail_str_list_server;

/*! \var head_str_list_log
    \brief head of the data linked list of the log thread
*/
extern str_node* head_str_list_log;

/*! \var tail_str_list_log
    \brief tail of the data linked list of the log thread
*/
extern str_node* tail_str_list_log;

/*! \var str_list_size_mysql
    \brief current size of the data linked list of the mysql thread
*/
extern int32_t str_list_size_mysql;

/*! \var str_list_size_server
    \brief current size of the data linked list of the server thread
*/
extern int32_t str_list_size_server;

/*! \var str_list_size_log
    \brief current size of the data linked list of the log thread
*/
extern int32_t str_list_size_log;

/*! \var serial_speed
    \brief currently set speed of the serial device, if negative then default
*/
extern int serial_speed;

/*! \var sql
    \brief large dynamically allocated c string for mysql data insertions
*/
extern char* sql;

/*! \var status
    \brief the global status variable for communicating erros across the program
*/
extern status_struct* status;

/*! \var use_log
    \brief setting flag to determine if logging functionality is to be used
*/
extern char use_log;

/*! \var use_mysql
    \brief setting flag to determine if database functionality is to be used
*/
extern char use_mysql;

/*! \var use_server
    \brief setting flag to determine if server functionality is to be used
*/
extern char use_server;

/*! \var server_port
    \brief the TCP port on which the server is listening
*/
extern int server_port;

/*! \var status_lock
    \brief mutex for atomic access to the status variable
*/
extern pthread_mutex_t status_lock;
/* END GLOBALS DEFINITIONS */


/* BEGIN FUNCTIONS DEFINITIONS */
/*! \fn add_str_node(str_node* str, char* line, double time)
    \brief Creates and appends and new data node to the data linked list.

    \param str tail of the data linked list
    \param line text content of the new data node
    \param time time of the data in Unix seconds (with fractions)
    \return new tail of the data linked list
*/
str_node* add_str_node(str_node* str, char* line, double time);

/*! \fn obtain_data(void* arg)
    \brief Serial data extracting thread.

    \param arg thread argument required from a POSIX thread
    \return return value required from a POSIX thread
*/
void* obtain_data(void* arg);


/*! \fn ignore_sigusr1(int signum)
    \brief Ignores custom user signal.

    \param signum argument required by interrupt handling functions
*/
void ignore_sigusr1(int signum);

/*! \fn millis()
    \brief Returns Unix time in milliseconds.

    \return Unix time in milliseconds
*/
uint64_t millis();

/*! \fn micros()
    \brief Returns Unix time in microseconds.

    \return Unix time in microseconds
*/
uint64_t micros();

/*! \fn delay(uint64_t millisec)
    \brief Blocks for a given number of milliseconds. Uses sleep.

    \param millisec the number of milliseconds for which to block
*/
void delay(uint64_t millisec);

/*! \fn hard_delay(uint64_t millisec)
    \brief Blocks for a given number of milliseconds. Uses hard loop.

    \param millisec the number of milliseconds for which to block
*/
void hard_delay(uint64_t millisec);

/*! \fn delayMicroseconds(uint64_t microsec)
    \brief Blocks for a given number of microseconds. Uses sleep.

    \param microsec the number of microseconds for which to block
*/
void delayMicroseconds(uint64_t microsec);

/*! \fn hard_delayMicroseconds(uint64_t microsec)
    \brief Blocks for a given number of microseconds. Uses hard loop.

    \param microsec the number of microseconds for which to block
*/
void hard_delayMicroseconds(uint64_t microsec);

/*! \fn server(void* arg)
    \brief Thread defining function for the TCP data server.

    \param arg argument required from a POSIX thread
    \return return value required from a POSIX thread
*/
void* server(void* arg);

/*! \fn free_str_nodes(str_node* str)
    \brief Frees the linked list of data nodes.

    \param str the head of the linked list to free
*/
void free_str_nodes(str_node* str);

/*! \fn handle_mysql_data(void* arg)
    \brief Thread defining function for mysql database functionality.

    \param arg argument required from a POSIX thread
    \return return required from a POSIX thread
*/
void* handle_mysql_data(void* arg);

/*! \fn handle_log_data(void* arg)
    \brief Thread defining function for logging functionality.

    \param arg argument required from a POSIX thread
    \return return required from a POSIX thread
*/
void* handle_log_data(void* arg);
/* END FUNCTIONS DEFINITIONS */

//error definitions
#define MYSQL_INIT_ERROR 1
#define MYSQL_TIMEOUT_SET_ERROR 2
#define MYSQL_CONNECT_ERROR 3
#define MYSQL_CREATE_DB_ERROR 4
#define MYSQL_USE_DB_ERROR 5
#define MYSQL_CREATE_TABLE_ERROR 6
#define LIST_NULL_ERROR 100
#define CLEAN_EXIT_ERROR 101 //not really an error, but a naming convention
#define MYSQL_INSERTION_ERROR 102
#define LOG_LOG_FD_ERROR 103
#define PIPE_COULD_NOT_OPEN_ERROR 200
#define PIPE_CLOSED_ERROR 201
#define SERVER_SOCKET_CREATE_ERROR 300
#define SERVER_SOCKET_BIND_ERROR 301
#define SERVER_LISTEN_ERROR 302
#define SERVER_ACCEPT_ERROR 303
#define SERVER_NON_BLOCKING_ERROR 304
#define SERVER_RECEIVE_ZERO_ERROR 305

#endif
