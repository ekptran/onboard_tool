#ifndef SERIAL_AND_DATA_SOURCE_H
#define SERIAL_AND_DATA_SOURCE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <signal.h>
#include <sys/time.h>
#include <time.h>
//#include <sys/socket.h>
//#include <netdb.h>
//#include <netinet/in.h>
//#include <sys/types.h>
//#include <mysql/mysql.h>
#include <stdint.h>
#include <pthread.h>
#include <poll.h>

#include "serdaq.h"
#include "atomic_read_write_tools.h"

/* BEGIN DEFAULT VALUES DEFINITIONS */
#define N (1<<10)
#define M (1<<9)
/* END DEFAULT VALUES DEFINITIONS */

/*! \file serial_and_data_source.h
    \brief Set of functions used for obtaining real-time data
*/

/*! \fn open_port(char* serial_path, int speed)
    \brief Used to open the serial port.

    \param serial_path the location of the serial port in the file system
    \param speed the speed of the serial port, negative for default
    \return the file descriptor of the serial port
*/
int open_port(char* serial_path, int speed);

/*! \fn wait_on_data(int fd, uint64_t millisec)
    \brief Allows to sleep to while waiting for data to appear on the serial.

    \param fd file descriptor of the serial port
    \param millisec number of milliseconds to wait before timing out
    \return indicating whether timed out or data arrived
*/
int wait_on_data(int fd, uint64_t millisec);

/*! \fn imprint_current_time(char* s)
    \brief Imprints the current time into a c string

    \param s c string into which current time is imprinted
*/
void imprint_current_time(char* s);

/*! \fn parse(char* buffer, size_t buff_len)
    \brief Parses the live byte stream of the serial into data linked lists

    \param buffer the current byte buffer of the serial data
    \param buff_len the current length of the buffer
    \return the number of bytes used from the byte buffer
*/
size_t parse(char* buffer, size_t buff_len);

/*! \fn add_str_node(str_node* str, char* line, double time)
    \brief Constructs and appends a data node to a data linked list.

    \param str the current tail of the data linked list
    \param line text to be placed in the new data node
    \param time the time stamp of the data node being constructed
    \return the tail head of the data linked list
*/
str_node* add_str_node(str_node* str, char* line, double time);

/*! \fn obtain_data(void* arg)
    \brief Thread defining function for real-time data reading from serial port.

    \param arg argument required from a POSIX thread
    \return return required from a POSIX thread
*/
void* obtain_data(void* arg);

#endif
